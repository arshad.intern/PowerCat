<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
	<style type="text/css">
		.img { 
			position: relative; 
			width: 100%; 
			height: 350px; 
			background-position: 50% 50%; 
			background-repeat: no-repeat; 
			background-size: cover; 
		} 
	</style>
</head>
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
		


			  <script src="js/responsiveslides.min.js"></script>
			  <script>
				$(function () {
				  $("#slider").responsiveSlides({
					auto: true,
					nav: true,
					speed: 500,
					namespace: "callbacks",
					pager: true,
				  });
				});
			  </script>

  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="./images/Brand New.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="./images/24_7.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="./images/Bandhan.jpg" alt="New york" style="width:100%;">
      </div>
    </div>
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <!-- <span class="fa arrow-left text-white"></span> -->
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      
      <span class="sr-only">Next</span>
    </a>
  </div>
	</div>
	</div>

<!--content-->
<div class="content">
	<div class="container">
	<div class="content-top">
		<h1>OUR PRODUCTS</h1>
		<div class="container mt-40">
            <div class="row mt-30">
				<?php
				include("admin/connection.php");
				$sql = mysqli_query($con, "SELECT * FROM `products` LIMIT 6") or die(mysqli_error($con));
				while ($row = mysqli_fetch_array($sql)) {
					$product_image = $row['product_image'];
					if ($product_image == "") {
						$product_image = "";
					} else {
						$product_image = "admin/products/".$product_image;
					}
					echo '
					<div class="col-md-4 col-sm-6">
						<div class="box19">
							<img class="img-responsive" src="'.$product_image.'" alt="" width="300" height="300">
							<div class="box-content">
								<ul class="icon">
									<li><a href="product_details.php?product_id='.$row[0].'"><svg class="bi bi-eye" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 001.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0014.828 8a13.133 13.133 0 00-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 001.172 8z" clip-rule="evenodd"/>
									<path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 100 5 2.5 2.5 0 000-5zM4.5 8a3.5 3.5 0 117 0 3.5 3.5 0 01-7 0z" clip-rule="evenodd"/>
									</svg></i></a></li>
								</ul>
								<h3 class="title">'.$row['product_title'].'</h3>
							</div>
						</div>
					</div>
					';
				}
				?>
			</div>
		</div>
	</div>
		
		<h1 class="text-center my-md-3">Generators</h1>

		<div class="clearfix"> </div>

		<center><div class="container mt-40 mt-5">
            <div class="row mt-30 justify-content-center">
                <div class="col-md-4 col-sm-6 ">
                    <div class="box19">
                        <img src="images/2.1-kw-4-kw.png" class="img-responsive" alt="">
                        <div class="box-content">
                            <ul class="icon">
                                <li><a href="pe.php"><svg class="bi bi-eye" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 001.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0014.828 8a13.133 13.133 0 00-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 001.172 8z" clip-rule="evenodd"/>
						<path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 100 5 2.5 2.5 0 000-5zM4.5 8a3.5 3.5 0 117 0 3.5 3.5 0 01-7 0z" clip-rule="evenodd"/>
						</svg></i></a></li>
                            </ul>
                            <h3 class="title">Petrol Genarator</h3>
                        </div>
                    </div>
                </div>
						<div class="col-md-4 col-sm-6">
							<div class="box19">
								<img src="images/40-kva-125-kva.png" class="img-responsive" alt="">
								<div class="box-content">
									<ul class="icon">
										<li><a href="de.php"><svg class="bi bi-eye" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 001.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0014.828 8a13.133 13.133 0 00-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 001.172 8z" clip-rule="evenodd"/>
						<path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 100 5 2.5 2.5 0 000-5zM4.5 8a3.5 3.5 0 117 0 3.5 3.5 0 01-7 0z" clip-rule="evenodd"/>
						</svg></a></li>
                            </ul>
                            <h3 class="title">Diesel Genarator</h3>
                        </div>
                    </div>
                </div>
            </div></center>
			<div class="content-bottom">
		<h1 class="text-center" style="margin-bottom: 60px;">OUR ACHIVEMENTS</h1>
		<ul>
			<li><a href="#"><img class="img-responsive" src="images/achivements/Award-3.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/achivements/Award-4.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/achivements/Award-6.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" style="width: 200px;height: 170px;" src="images/achivements/award-20.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" style="width: 240px;height: 170px;"src="images/achivements/P2.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/achivements/P5.jpg" alt=""></a></li>
		<div class="clearfix"> </div>
		</ul>
		<h1 class="text-center" style="margin-bottom: 60px;margin-top: 60px;">OUR CLIENTS</h1>
		<ul>
			<li><a href="#"><img class="img-responsive" src="images/clients/bharath-bank.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/Canara-Bank.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/SDM.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/Infosys.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/ICICI_Bank.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/syndicate-bank.jpg" alt=""></a></li>
		<div class="clearfix"> </div>
		</ul>
	</div>
        </div>
	

</div>
<?php include("inc/footer.php"); ?>
</body>
</html>
			