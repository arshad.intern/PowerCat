<?php include("inc/session.php"); ?>

<!DOCTYPE html>
<html lang="en">

<head>

   <?php include("inc/head.php"); ?>

</head>

<body>
   <?php include("inc/topmenu.php"); ?>
    <!-- Navigation -->
      <div class="container-fluid main-container">
        <div class="col-md-2 sidebar">
            <?php include("inc/sidemenu.php"); ?>
        </div>
        <div class="col-md-10 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Manage Subscribers
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Sno</th>
                                        <th>Email</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                        include("connection.php");
                                        $i = 1;
                                        $sql = mysqli_query($con, "SELECT * FROM `newsletter` WHERE email = '$news_email'") or die(mysqli_error($con));
                                        while($row = mysqli_fetch_array($sql))
                                        {
                                            echo '<tr class="record">

                                                <td>'.$i++.'</td>
                                                <td>'.$row['email'].'</td>
                                                <td>
                                                    <a href="#" id="'.$row['news_id'].'" data-toggle="tooltip" title="Delete subscriber" class="btn btn-danger btn-sm delbutton"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>';
                                        }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
       <?php include("inc/footer.php"); ?>
    </div>
    <!- - jQuery Version 1.11.1 -->
    <?php include("inc/bottom.php"); ?>

        <script type="text/javascript">
    $(function() {
        $('body').on('click', '.delbutton', function() {
            //Save the link in a variable called element
            var element = $(this);
            //Find the id of the link that was clicked
            var del_id = element.attr("id");
            //Built a url to send
            var info = 'id=' + del_id + '&delete';
            if(confirm("Sure you want to delete?"))
            {
                $.ajax({
                     type: "GET",
                     url: "org_val.php",
                     data: info,
                     success: function(){ 
            }
         });
         $(this).parents(".record").animate({ backgroundColor: "#fbc7c7" }, "fast")
        .animate({ opacity: "hide" }, "slow");
            }
            return false;
        });
    });
</script>

</body>
</html>
