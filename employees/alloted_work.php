<?php include("./inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

<?php include("./inc/head.php") ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

        <!-- Sidebar -->
        <?php include('./inc/sidebar.php') ?>
    <!-- End of Sidebar -->
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

<!-- Topbar -->
<?php include('./inc/topDashboard.php'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800 my-4">Alloted Works</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            
            <div class="card-body">
              <div class="table-responsive">
              <div class="panel-body">
					<?php
						if(isset($_GET['rep_generated']))
						{
							echo '<div class="alert alert-danger">
									<a href="#" class="close" data-dismiss="alert">&times; </a>
									<p>Alreadyready Report Generated On this date</p>
								</div>';
						}
					?>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Sno</th>
                            <th>SuperVisor</th>
                            <th>Complaint</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                            include("../admin/connection.php");
                            $i = 1;
							$k = 1;
                            $sql = mysqli_query($con, "SELECT * FROM `alloted_work` INNER JOIN `supervisor` ON `alloted_work`.sup_email = `supervisor`.sup_email INNER JOIN `complaint_registar` ON `alloted_work`.comp_id = `complaint_registar`.complaint_id WHERE `alloted_work`.pulse_id = '$mr_pulseuidno'") or die(mysqli_error($con));
                            while($row = mysqli_fetch_array($sql))
                            {
                                echo '<tr>
                                        <td>'.$i++.'</td>
                                        <td>'.$row['sup_name'].'</td>
                                        <td><a href="view_complaint.php?id='.$row['complaint_id'].'">'.$row['engine_number'].'</a></td>
                                        <td>'.$row['work_date'].'</td>';

                                        $query = mysqli_query($con, "SELECT * FROM `comp_report` WHERE comp_id = '".$row['complaint_id']."'") or die(mysqli_error($con));
                                        $count = mysqli_num_rows($query);

                                        if($count > 0)
                                        {
                                            echo '<td><label class="label label-primary"><i class="fa fa-check fa-fw"></i>Report Sent</label></td>';
                                        }
                                        else
                                        {
                                            echo ' <td>
                                                        <a href="#" data-toggle="modal" data-target="#myModal'.$k.'" class="btn btn-warning btn-xs btn-block">Send Report</a>
                                                    </td>';
                                        }
										
										
										$rep_query = mysqli_query($con, "SELECT * FROM `comp_report`") or die(mysqli_error($con));
										$rep_count = mysqli_num_rows($rep_query);


                                       
                                    echo '</tr>

                                     <!-- Modal -->
                                    <div id="myModal'.$k.'" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Generate Report</h4>
                                          </div>
                                          <div class="modal-body">
                                            <form name="" method="post" action="send_report.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Date</label>
                                                            <input type="text" name="rep_date" class="form-control" value="'.date('Y-m-d').'" readonly/>
                                                            <input type="hidden" name="comp_id" class="form-control" value="'.$row['complaint_id'].'" readonly/>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>ESN</label>
                                                            <input type="text" value="'.$row['engine_serial_number'].'" readonly name="rep_esn" class="form-control" required placeholder="ESN Number"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Task Close</label>
                                                            <select name="task_close" class="form-control" required>
																<option>Yes</option>
																<option>No</option>
															</select>
                                                        </div>
                                                    </div>

                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Rep Number</label>
                                                            <input type="number" name="rep_number" value="'.$rep_count.'" readonly class="form-control" required/>
                                                        </div>
                                                    </div>
                                                </div>

                                                 <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Bill</label>
                                                            <select name="bill" class="form-control" required>
																<option>Yes</option>
																<option>No</option>
															</select>
                                                        </div>
                                                    </div>

                                                     <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Transport</label>
                                                           <select name="transport" class="form-control" required>
																<option>Yes</option>
																<option>No</option>
															</select>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="submit" name="send_rep" class="btn btn-default">Send Report</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                          </form>
                                        </div>

                                      </div>
                                    </div>';
									
									$k++;
                            }
                        ?>
                    </tbody>    
                    </table>
                    
                </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php include('inc/footer.php') ?>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>
  

</body>

</html>
