<?php include("./inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

<?php include("./inc/head.php") ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php include('./inc/sidebar.php') ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
            <?php include('./inc/topDashboard.php'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
        <?php
                           
                           include("connection.php");
                           $id=$_GET['id'];
                           $sql_view=mysqli_query($con,"select * from `complaint_registar` where `complaint_id`='$id'");
                           $row_view=mysqli_fetch_array($sql_view);
                       ?>
        <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">View Complaints</h1>
            </div>
        <!-- End of Page Heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form name="" method="post" action="c_val.php">
                                <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Zone:</label>
                                                    <?php echo $row_view['zone']; ?>
                                                </div>
                                            </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Dealer Name:</label>
                                                <?php echo $row_view['dealer_name']; ?>
                                            </div>
                                        </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>GOEM:</label>
                                                    <?php echo $row_view['goem']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Dealer number:</label>
                                                    <?php echo $row_view['dealer_no']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Account name:</label>
                                                    <?php echo $row_view['account_name']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Phone Number:</label>
                                                    <?php echo $row_view['phone_number']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>SR Number:</label>
                                                    <?php echo $row_view['sr_number']; ?>
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>SR_Status:</label>
                                                            <?php echo $row_view['sr_status']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>SR_Type:</label>
                                                    <?php echo $row_view['sr_type']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>SR_subtype:</label>
                                                        <?php echo $row_view['sr_subtype']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Engine Number:</label>
                                                    <?php echo $row_view['engine_number']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Engine Code Serviced:</label>
                                                            <?php echo $row_view['engine_codeserviced']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Engine series Service:</label>
                                                            <?php echo $row_view['engine_seriesserviced']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Segment:</label>
                                                        <?php echo $row_view['segment']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>KVA:</label>
                                                            <?php echo $row_view['kva']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Problem Description:</label>
                                                            <?php echo $row_view['problem_description']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Installation site Address:</label>
                                                            <?php echo $row_view['installation_siteaddress']; ?>
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Installation City:</label>
                                                            <?php echo $row_view['installation_city']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>SR Due Date:</label>
                                                            <?php echo $row_view['sr_duedate']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Aging Days:</label>
                                                            <?php echo $row_view['aging_days']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Aging Range:</label>
                                                            <?php echo $row_view['aging_range']; ?>
                                                </div>                               
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Warranty Expiry Date:</label>
                                                    <?php echo $row_view['warranty_expirydate']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Engine Serial Number:</label>
                                                            <?php echo $row_view['engine_serial_number']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                         <label>Branch:</label>
                                                            <?php echo $row_view['Branch']; ?>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Customer Name:</label>
                                                            <?php echo $row_view['customer_name']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Instance Id:</label>
                                                            <?php echo $row_view['instance_id']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Branch Code:</label>
                                                            <?php echo $row_view['branch_code']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Physical Status:</label>
                                                            <?php echo $row_view['physical_status']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Location:</label>
                                                            <?php echo $row_view['location']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Deputed Technician:</label>
                                                            <?php echo $row_view['depute_tech']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Deputed Date:</label>
                                                            <?php echo $row_view['depute_date']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Follow Up Co-ordinators:</label>
                                                            <?php echo $row_view['follow_upcoordinators']; ?>
                                                </div>
                                            </div>
                                           <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Complaint Recieved By:</label>
                                                            <?php echo $row_view['complaint_recievedby']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Recomended By:</label>
                                                            <?php echo $row_view['recommendedby']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Proprietory Spare Parts:</label>
                                                               <?php echo $row_view['proprietory_spareparts']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Remark:</label>
                                                            <?php echo $row_view['remark']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Spares Bill No:</label>
                                                        <?php echo $row_view['sbn']; ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Labour Bill No:</label>
                                                        <?php echo $row_view['lbn']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <?php
                                            $qqq = mysqli_query($con, "SELECT * FROM `alloted_work` INNER JOIN `movement_register` ON `alloted_work`.pulse_id = `movement_register`.mr_pulseuidno WHERE   `alloted_work`.comp_id = '$id'") or die(mysqli_error($con));
                                            $count = mysqli_num_rows($qqq);

                                            if($count > 0)
                                            {
                                                $row = mysqli_fetch_array($qqq);
                                                echo '  <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="well">
                                                                    <p>Work has been Alloted to: '.$row['mr_name'].'</p>
                                                                </div>
                                                            </div>
                                                        </div>';
                                            }

                                        ?>
 -->
                                </form>
                              
                                <a href="export-c.php?complaint_id=<?php echo $row_view[0];?>" class="btn btn-success">Export To Excel</a>
                            </form>
                            </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php include("./inc/footer.php") ?>                     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="./logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>


  <script>
            $('#area').restrictLength($('#maxlength'));
        </script>
</body>

</html>