<?php include("./inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

<?php include("./inc/head.php") ?>
<script>
    function validatePassword() {
    var currentPassword,newPassword,confirmPassword,output = true;

    currentPassword = document.frmChange.currentPassword;
    newPassword = document.frmChange.newPassword;
    confirmPassword = document.frmChange.confirmPassword;

    if(!currentPassword.value) {
        currentPassword.focus();
        document.getElementById("currentPassword").innerHTML = "required";
        output = false;
    }
    else if(!newPassword.value) {
        newPassword.focus();
        document.getElementById("newPassword").innerHTML = "required";
        output = false;
    }
    else if(!confirmPassword.value) {
        confirmPassword.focus();
        document.getElementById("confirmPassword").innerHTML = "required";
        output = false;
    }
    if(newPassword.value != confirmPassword.value) {
        newPassword.value="";
        confirmPassword.value="";
        newPassword.focus();
        document.getElementById("confirmPassword").innerHTML = "not same";
        output = false;
    }   
    return output;
}
</script>
</head>

<body id="page-top">
         <?php
        include("connection.php");
        if (count($_POST)>0) {
            $result = "SELECT * from `user` WHERE u_email='$user_email'";
            $m=mysqli_query($con, $result);
            $row=mysqli_fetch_array($m);
            if ($_POST["currentPassword"] == $row["u_password"]) {
                $e="UPDATE user set u_password='" . $_POST["newPassword"] . "' WHERE u_email='$user_email'";
                $res=mysqli_query($con, $e);
                $message = "Password Changed";
            } else {
                $message = "Current Password is not correct";
            }
        }
    ?>   

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php include('./inc/sidebar.php') ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
            <?php include('./inc/topDashboard.php'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
        <h1 class="h3 mb-2 text-gray-800 my-4">Change Password</h1>
          <!-- Content Row -->
          <form name="frmChange" method="post" action="" onSubmit="return validatePassword()">

<?php if (isset($message)) {
        echo '<div class="alert alert-info">'.$message.'</div>';
    }
?>
<table class="table table-bordered">
    <tr class="tableheader">
        <td colspan="2"><h5> Change Password </h5></td>
    </tr>
    <tr>
        <td width="40%"><label> Current Password </label></td>
        <td width="60%"><input type="password" name="currentPassword" class="form-control"/><span id="currentPassword"  class="required"></span></td>
    </tr>
    <tr>
        <td><label> New Password </label></td>
        <td><input type="password" name="newPassword" class="form-control"/><span id="newPassword" class="required"></span></td>
    </tr>
    <tr>
        <td><label> Confirm Password </label></td>
        <td><input type="password" name="confirmPassword" class="form-control"/><span id="confirmPassword" class="required"></span></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
    </tr>
</table>
</form>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php include("./inc/footer.php") ?>                     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="./logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>


  <script>
            $('#area').restrictLength($('#maxlength'));
        </script>
</body>

</html>
