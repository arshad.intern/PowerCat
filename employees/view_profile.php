<?php include("./inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>

<?php include("./inc/head.php") ?>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php include('./inc/sidebar.php') ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
            <?php include('./inc/topDashboard.php'); ?>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">View Profile</h1>
            </div>
        <!-- End of Page Heading -->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                                include("../admin/connection.php");
                                $sql = mysqli_query($con, "SELECT * FROM `movement_register` WHERE  mr_pulseuidno = '$mr_pulseuidno'") or die(mysqli_error($con));
                                $row = mysqli_fetch_array($sql);

                            ?>
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <td><?php echo $row['mr_name']; ?></td>
                                </tr>

                                <tr>
                                    <th>Qualification</th>
                                    <td><?php echo $row['mr_qual']; ?></td>
                                </tr>
                                 <tr>
                                    <th>Designation</th>
                                    <td><?php echo $row['mr_desig']; ?></td>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <td><?php echo $row['mr_paddress']; ?></td>
                                </tr>
                                <tr>
                                    <th>Father/Mother Name:</th>
                                    <td><?php echo $row['mr_pname']; ?></td>
                                     
                                </tr>
                                <tr>
                                    <th>Permanent Address:</th>
                                    <td><?php echo $row['mr_paddress']; ?></td>
                                </tr>
                                <tr>
                                    <th>Seccurity Doc&amp;No:</th>
                                    <td><?php echo $row['mr_securitydocno']; ?></td>
                                </tr>
                                <tr>
                                    <th>Official Tel No:</th>
                                    <td><?php echo $row['mr_oftelno']; ?></td>
                                </tr>
                                <tr>
                                    <th>ESI Nos:</th>
                                    <td><?php echo $row['mr_esino']; ?></td>
                                </tr>
                                <tr>
                                    <th>Date of Birth</th>
                                    <td><?php echo $row['mr_dob']; ?></td>                                           
                                </tr>
                                <tr>
                                    <th><label>Date of Join</th>
                                    <td><?php echo $row['mr_doj']; ?></td>                                           
                                </tr>
                                <tr>
                                    <th>Comapany EID No:</td>
                                    <td> <?php echo $row['mr_comeidno']; ?></td>
                                </tr>
                                <tr>
                                    <td>Comapany EID No:</label></td>
                                    <td> <?php echo $row['mr_comeidno']; ?></td>
                                </tr>
                                <tr>
                                    <th>KOEL Ticket No:</tH>
                                    <td><?php echo $row['mr_koelticketno']; ?></td>
                                </tr>
                                <tr>
                                    <td>PULSE Uid No:</tH>
                                    <td> <?php echo $row['mr_pulseuidno']; ?></td>
                                </tr>
                                <tr>
                                    <th>Tool Kit Issued By:</tH>
                                    <td> <?php echo $row['mr_tkissuedby']; ?></td>
                                </tr>


                            </table>
                            </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <?php include("./inc/footer.php") ?>                     
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="./logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>


  <script>
            $('#area').restrictLength($('#maxlength'));
        </script>
</body>

</html>
