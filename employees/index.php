<!DOCTYPE html>
<html lang="en">

<head>

<?php include("./inc/head.php") ?>
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9 my-5">

        <div class="card o-hidden border-0 shadow-lg my-5 align-self-lg-center my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4 mt-5">Welcome Back!</h1>
                  </div>
                  <form class="user" name="" method="post" action="">
                    <div class="form-group">
                      <input type="text" for="defaultForm-email" name="pulse_id" placeholder="PULSE ID" class="form-control form-control-user" id="exampleInputPassword">
                    </div>
                    <!-- <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>F
                    </div> -->
                    <button type="submit" name="login_btn" value="Login" class="btn btn-primary btn-user btn-block">
                      Login
                    </button>
                    <!-- <a href="index.html" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> Login with Google
                    </a>
                    <a href="index.html" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                    </a> -->
                  </form>
                  <p>
                  <p>
          <?php
        	include("../admin/connection.php");
        	if(isset($_POST['login_btn']))
        	{
        		$mr_pulseuidno = mysqli_real_escape_string($con, $_POST['pulse_id']);

        		$query = mysqli_query($con, "SELECT * FROM `movement_register` WHERE mr_pulseuidno = '$mr_pulseuidno'") or die(mysqli_error($con));
        		$row = mysqli_fetch_array($query);

        		if($row)
        		{
        			session_start();
        			$_SESSION['mr_pulseuidno'] = $row['mr_pulseuidno'];
        			header("location:home.php?success");
        		}
        		else
        		{
                    // echo '<h3 class="invalid" style="color:red; text-align:center;">Invalid Email and Password... </h3>';
        			echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Hey User!</strong> Invalid PULSE Id
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>';

        		}
        	}
        ?>
        </p>
        </p>
                  <hr>
                  <!-- <div class="text-center">
                    <a class="small" href="forget.php">Forgot Password?</a>
                  </div> -->
                  <div class="text-center">
                    <a class="small" href="../admin/">Admin Login!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
