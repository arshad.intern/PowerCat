

<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.php">
  <div class="sidebar-brand-icon">
    <img src="./images/ppelogo.jpg" class="logoborder" height="50" width="50" />
  </div>
  <div class="sidebar-brand-text mx-3">Power Cat </div>
</a>

<!-- Divider -->
<hr class="sidebar-divider my-0">

<!-- Nav Item - Dashboard -->
<li class="nav-item active">
  <a class="nav-link" href="home.php">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider">

<li class="nav-item">
  <a class="nav-link"href="alloted_work.php">
  <i class="far fa-address-book"></i>
    <span>  Alloted Work</span></a>
</li>
<hr class="sidebar-divider">
<li class="nav-item">
<a class="nav-link" href="reports.php">
<i class="fas fa-users"></i>
  <span>  View Reports</span></a>
</li>
<!-- Sidebar Toggler (Sidebar) -->
<hr class="sidebar-divider">
<div class="text-center d-none d-md-inline">
  <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

</ul>