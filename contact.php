<?php include('inc/session.php')?>
<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head>
<body>
<!--header-->
	<?php include("inc/topmenu.php"); ?>
	
<!--content-->
<div class="contact">
			
			<div class="container">
				<h1>Contact</h1>
			<div class="contact-form">
				
				<div class="col-md-8 contact-grid">
					<form action="f_val.php" method="POST">	
						<input type="text" placeholder="Name" name="f_name" required>
					
						<input type="text" placeholder="Email" name="f_email" required>
						<input type="text" placeholder="Phone Number" name="f_num" required>
						
						<textarea cols="77" rows="6" placeholder="Message" name="f_mes"required></textarea>
						<div class="send">
							<input type="submit" value="Send" name="send_mes">
						</div>
					</form>
				</div>
				<div class="col-md-4 contact-in">

						<div class="address-more">
						<h4>Address</h4>
							<p>Prasad Power Engineers,</p>
							<p>Industrial Estate,Yeyyadi,</p>
							<p>Mangaluru – 575 008.. </p>
						</div>
						<div class="address-more">
						<h4>Contact Info</h4>
							<p>Free Phone :+91 9741472213</p>
							<p>Telephone :0824-2455417</p>
							<p>Fax :+1 078 4589 2456</p>
							<p>Email:<a href="mailto:prasadpowerengineers@gmail.com">ppemanglore@gmail.com</a></p>
						</div>
					
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="">
				<iframe style="width:100%;" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJQxsilxBaozsRqUabZggmYAU&key=AIzaSyCrBCzP5rfUBK_wCjOd8xcd74XTWxKPOMg" allowfullscreen></iframe>
			</div>
		</div>
		
	</div>
<!--//content-->
<?php include("inc/footer.php");?>
</body>
			