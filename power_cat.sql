-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2018 at 03:35 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `power_cat`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `a_name` varchar(20) NOT NULL,
  `a_email` varchar(30) NOT NULL,
  `a_phone` bigint(20) NOT NULL,
  `a_address` varchar(100) NOT NULL,
  `a_password` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `a_name`, `a_email`, `a_phone`, `a_address`, `a_password`) VALUES
(1, 'Administrator', 'admin@gmail.com', 9448348128, 'Mangalore123', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `alloted_work`
--

CREATE TABLE IF NOT EXISTS `alloted_work` (
  `work_id` int(11) NOT NULL AUTO_INCREMENT,
  `pulse_id` varchar(60) NOT NULL,
  `sup_email` varchar(60) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `work_date` varchar(25) NOT NULL,
  PRIMARY KEY (`work_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `alloted_work`
--

INSERT INTO `alloted_work` (`work_id`, `pulse_id`, `sup_email`, `comp_id`, `work_date`) VALUES
(1, 'A1002', 'arshad@gmail.com', 17, '2018-04-04'),
(2, 'A1003', 'arshad@gmail.com', 16, '2018-04-04'),
(3, 'A1002', 'arshad@gmail.com', 15, '2018-04-04'),
(4, 'A1002', 'arshad@gmail.com', 14, '2018-04-04');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_receivers`
--

CREATE TABLE IF NOT EXISTS `complaint_receivers` (
  `cr_id` int(11) NOT NULL AUTO_INCREMENT,
  `cr_name` varchar(30) NOT NULL,
  `cr_phone` bigint(15) NOT NULL,
  `cr_dob` date NOT NULL,
  `cr_email` varchar(50) NOT NULL,
  `cr_gender` text NOT NULL,
  `cr_address` varchar(100) NOT NULL,
  `cr_quali` varchar(10) NOT NULL,
  `cr_exp` varchar(10) NOT NULL,
  `cr_password` varchar(30) NOT NULL,
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `complaint_receivers`
--

INSERT INTO `complaint_receivers` (`cr_id`, `cr_name`, `cr_phone`, `cr_dob`, `cr_email`, `cr_gender`, `cr_address`, `cr_quali`, `cr_exp`, `cr_password`) VALUES
(1, 'manoj', 9448348128, '2018-03-19', 'm@gmail.com', 'Female', 'Manglore', 'Bcom', 'none', ' manoj');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_registar`
--

CREATE TABLE IF NOT EXISTS `complaint_registar` (
  `complaint_id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(20) NOT NULL,
  `dealer_name` varchar(30) NOT NULL,
  `goem` varchar(30) NOT NULL,
  `dealer_no` int(30) NOT NULL,
  `account_name` varchar(30) NOT NULL,
  `phone_number` bigint(30) NOT NULL,
  `sr_number` varchar(30) NOT NULL,
  `sr_status` varchar(30) NOT NULL,
  `sr_type` varchar(30) NOT NULL,
  `sr_subtype` varchar(30) NOT NULL,
  `engine_number` varchar(30) NOT NULL,
  `engine_codeserviced` varchar(30) NOT NULL,
  `engine_seriesserviced` varchar(30) NOT NULL,
  `segment` varchar(30) NOT NULL,
  `kva` varchar(30) NOT NULL,
  `problem_description` varchar(100) NOT NULL,
  `installation_siteaddress` varchar(100) NOT NULL,
  `installation_city` varchar(100) NOT NULL,
  `sr_duedate` varchar(30) NOT NULL,
  `aging_days` varchar(30) NOT NULL,
  `aging_range` varchar(30) NOT NULL,
  `warranty_expirydate` varchar(30) NOT NULL,
  `engine_serial_number` varchar(50) NOT NULL,
  `Branch` text NOT NULL,
  `customer_name` text NOT NULL,
  `instance_id` int(15) NOT NULL,
  `branch_code` varchar(100) NOT NULL,
  `physical_status` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `depute_tech` varchar(100) NOT NULL,
  `depute_date` varchar(30) NOT NULL,
  `follow_upcoordinators` varchar(100) NOT NULL,
  `complaint_recievedby` varchar(20) NOT NULL,
  `recommendedby` varchar(100) NOT NULL,
  `proprietory_spareparts` text NOT NULL,
  `remark` varchar(100) NOT NULL,
  `sbn` varchar(40) NOT NULL,
  `lbn` varchar(40) NOT NULL,
  PRIMARY KEY (`complaint_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `complaint_registar`
--

INSERT INTO `complaint_registar` (`complaint_id`, `zone`, `dealer_name`, `goem`, `dealer_no`, `account_name`, `phone_number`, `sr_number`, `sr_status`, `sr_type`, `sr_subtype`, `engine_number`, `engine_codeserviced`, `engine_seriesserviced`, `segment`, `kva`, `problem_description`, `installation_siteaddress`, `installation_city`, `sr_duedate`, `aging_days`, `aging_range`, `warranty_expirydate`, `engine_serial_number`, `Branch`, `customer_name`, `instance_id`, `branch_code`, `physical_status`, `location`, `depute_tech`, `depute_date`, `follow_upcoordinators`, `complaint_recievedby`, `recommendedby`, `proprietory_spareparts`, `remark`, `sbn`, `lbn`) VALUES
(1, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 0, 'B.UDAY KUMAR SHETTY', 9901224245, '203384773', 'Open', 'CSP', 'M1', '1721506', 'T4.2407...', 'R810', 'IND', '20', 'WARRANTY SERVICE CHECKS / CSP', 'MALAHALLI HOUSE, KOTA POST, KUNDAPURA TALUK, Udupi, KARNATAKA', 'Udupi', '09-03-18', '17', '15 - 30 Days', '08-01-19', 'T4.2407/1721506', 'Manglore', 'Arshad', 78738, 'Manglore1', 'open', 'Manglore', 'Aravind', '21/03/14', 'akash', 'arshad', 'arshad', 'FIP', 'No', 'A1001', 'A2001'),
(10, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'CHANDRA SHEKAR', 9008371147, '203441877', 'Open', 'CSP', 'M1', '1721599', 'T4.2407...', 'R810', 'IND', '', 'WARRANTY SERVICE CHECKS / CSP', 'Kundapura., ., KARNATAKA', '', '19-03-2018', '7', '2 - 7 Days', '18-01-2019', 'T4.2407/1721599', '', 'Ashiq', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(11, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'MANOJ MANOJ', 9686336817, '202878763', 'Open', 'Post Warranty', 'Preventive Maintenance', '1520324', 'T4.2407...', 'R810', 'IND', '', 'PREVENTIVE MAINTENANCE - POST WARRANTY', 'Ankadakatte, Koteswar Post, Kundapur, KARNATAKA', '', '12-03-18', '14', '7 - 15 Days', '31-10-2016', 'T4.2407/1520324', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'Mr. K.SADANANDA SHETTY.,', 9448459120, '203632955', 'Open', 'CSP', 'C1', '1820314', 'T4.2407...', 'R810', 'IND', '', 'WARRANTY SERVICE CHECKS / CSP', 'P.W.D.CLASS 1 CONTRACTOR, MALAVIKA CONSTRUCTION,KEDOOR, KUNDAPUR TALUK, UDUPI, KARNATAKA', '', '22-03-2018', '4', '2 - 7 Days', '15-03-2019', 'T4.2407/1820314', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(13, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'Mr. LAXMAN CHAVAN.,', 9620435695, '203589705', 'Task Close', 'CSP', 'C1', '1820198', 'T4.2407...', 'R810', 'IND', '', 'WARRANTY SERVICE CHECKS / CSP', 'Kundapura, Udupi, KARNATAKA', '', '03-03-18', '23', '15 - 30 Days', '24-02-2019', 'T4.2407/1820198', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(14, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'N SHIVARAMAKRISHNA REDDY', 7795561777, '203151447', 'Open', 'CSP', 'M2', '1721117', '4H.3306...', 'R1040', 'IND', '', 'WARRANTY SERVICE CHECKS / CSP', 'Charmadi ghat, Chikmagalur, KARNATAKA', 'Chikkamagaluru', '13-03-2018', '13', '7 - 15 Days', '13-11-2018', '4H.3306/1721117', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(15, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'Poorappa Chawan', 9448024454, '203394314', 'Open', 'CSP', 'M1', '1721568', 'T4.2407...', 'R810', 'IND', '', 'WARRANTY SERVICE CHECKS / CSP', 'Site:Bhadravathi, Shimoga, KARNATAKA', 'Sagara', '13-03-2018', '13', '7 - 15 Days', '12-01-19', 'T4.2407/1721568', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(16, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'SHARATH SHETTY', 8296302620, '202995368', 'Open', 'CSP', 'M3', '1620689', 'T4.2407...', 'R810', 'IND', '', 'WARRANTY SERVICE CHECKS / CSP', 'Class-1., PWD Contractor, Udupi, KARNATAKA', 'Udupi', '10-03-18', '16', '15 - 30 Days', '19-03-2018', 'T4.2407/1620689', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(17, 'ZO_South II (CS)', 'Prasad Power Engineers - Manga', 'Ajax Fiori Engineering (India)', 420157, 'SURYA INFRASTRUCTURES & EQUIPM', 9576423616, '203171440', 'Open', 'CSP', 'M2', '1720919', '4H.3306...', 'R1040', 'IND', '', 'WARRANTY SERVICE CHECKS / CSP', 'SURYA INFRASTRUCTURES & EQUIPMENTS Site Kundapura, Udupi, KARNATAKA', '', '15-03-2018', '11', '7 - 15 Days', '15-11-2018', '4H.3306/1720919', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', ''),
(18, '', 'Select', '', 0, '', 0, '', 'Select', 'Select', 'Select', '', '', '', 'Select', '', '', '', '', '', '', '', '', '', 'Select', '', 0, '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `comp_report`
--

CREATE TABLE IF NOT EXISTS `comp_report` (
  `rep_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_id` int(15) NOT NULL,
  `rep_date` date NOT NULL,
  `rep_esn` varchar(25) NOT NULL,
  `task_close` varchar(10) NOT NULL,
  `rep_number` varchar(25) NOT NULL,
  `bill` varchar(25) NOT NULL,
  `transport` varchar(25) NOT NULL,
  `pulse_id` varchar(35) NOT NULL,
  PRIMARY KEY (`rep_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `comp_report`
--

INSERT INTO `comp_report` (`rep_id`, `comp_id`, `rep_date`, `rep_esn`, `task_close`, `rep_number`, `bill`, `transport`, `pulse_id`) VALUES
(1, 17, '2018-04-04', '4H.3306/1720919', 'Yes', '0', 'Yes', 'Yes', 'A1002'),
(2, 16, '2018-04-04', 'T4.2407/1620689', 'Yes', '1', 'Yes', 'Yes', 'A1003');

-- --------------------------------------------------------

--
-- Table structure for table `dealer`
--

CREATE TABLE IF NOT EXISTS `dealer` (
  `d_id` int(11) NOT NULL AUTO_INCREMENT,
  `dealer_name` text NOT NULL,
  `branch_code` varchar(10) NOT NULL,
  PRIMARY KEY (`d_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `dealer`
--

INSERT INTO `dealer` (`d_id`, `dealer_name`, `branch_code`) VALUES
(1, 'Prasad Power Engineers Mangluru', '420157_1'),
(2, 'Prasad Power Engineers Udupi', '420157_2'),
(5, 'Prasad Power Engineers Shivamogga\r\n', '420157_3'),
(6, 'Prasad Power Engineers Chikmagalur\r\n', '420157_4'),
(7, 'Prasad Power Engineers Puttur', '420157_5');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(20) NOT NULL,
  `emp_phone` bigint(20) NOT NULL,
  `emp_dob` date NOT NULL,
  `emp_email` varchar(30) NOT NULL,
  `emp_gender` text NOT NULL,
  `emp_address` varchar(100) NOT NULL,
  `emp_desig` varchar(20) NOT NULL,
  `emp_qual` varchar(10) NOT NULL,
  `emp_doj` date NOT NULL,
  `emp_exp` varchar(20) NOT NULL,
  `emp_trin` varchar(20) NOT NULL,
  `emp_sal` bigint(10) NOT NULL,
  `emp_pf` bigint(10) NOT NULL,
  `emp_hra` bigint(10) NOT NULL,
  `emp_password` varchar(20) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `emp_name`, `emp_phone`, `emp_dob`, `emp_email`, `emp_gender`, `emp_address`, `emp_desig`, `emp_qual`, `emp_doj`, `emp_exp`, `emp_trin`, `emp_sal`, `emp_pf`, `emp_hra`, `emp_password`) VALUES
(2, 'Ashraf', 8951100737, '2018-02-07', 'l@gmail.com', 'male', '   banglore ', 'manager', 'bbm', '2018-02-12', 'no', 'no', 10000, 5, 5, ' '),
(3, 'Jibin', 9448348128, '2018-03-11', 'jibin@gmail.om', 'female', '   Mangaluru ', 'Maneger', 'Bcom', '2018-03-12', '1.6 years', 'none', 15000, 4, 2, ' '),
(4, 'Arshiya', 9448348128, '2018-03-12', 'arshiya@gmail.com', 'male', 'kalasa,chikkamaglooru', 'Manager', 'Bsc', '2018-03-19', 'Fresher', 'No', 30000, 5, 4, ' ');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `f_id` int(5) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) NOT NULL,
  `f_email` varchar(30) NOT NULL,
  `f_num` int(15) NOT NULL,
  `f_mes` varchar(100) NOT NULL,
  PRIMARY KEY (`f_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`f_id`, `f_name`, `f_email`, `f_num`, `f_mes`) VALUES
(1, 'Ashiq', 'aq@gmail.com', 2147483647, 'Good'),
(2, 'Arshad', 'arsh@gmail.com', 944838128, 'Thank You..'),
(3, 'ar', 'a@gjsn', 998999, 'mmnaa'),
(4, 'ar', 'a@gjsn', 998999, 'mmnaa');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `user_role` int(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`login_id`, `name`, `email`, `phone`, `user_role`, `address`, `password`) VALUES
(1, 'Administrator', 'admin@gmail.com', 9448348128, 1, 'Mangalore', 'admin'),
(4, 'Prasad Power Enginee', 'ppe@gmail.com', 826356186, 2, 'Yayyadi, manglore', 'ppe12345'),
(11, 'Prasad Power Enginee', 'ppe@gmail.com', 826356186, 2, 'Yayyadi, manglore', 'ppe12345'),
(13, 'manoj', 'm@gmail.com', 8897274382, 3, 'manglore', 'manoj'),
(14, 'Arshad', 'arshad@gmail.com', 9448348128, 4, '  Mangalore', 'arshad'),
(15, 'Abhinandhan', 'arshadarsu028@gmail.com', 9448348128, 4, '  Managluru', 'arshadarsu0283856'),
(16, 'KOEL CARE', 'koelcare@gmail.com', 9900990099, 5, 'Mangalore', 'koelcare8'),
(17, 'PPE TRADRERS', 'ppet@gmail.com', 9900889900, 5, 'Manglore ', 'ppet4'),
(21, 'ashish', 'as@gmail.com', 6655665566, 3, 'manglore', 'as8707'),
(22, 'Ashraf', 'mashraf@gmail.com', 8951100737, 4, '  Manglore', 'mashraf7159'),
(23, 'manoj', 'm@gmail.com', 9448348128, 3, 'Manglore', 'm3406'),
(25, 'karkera Org pvt lmt', 'karkera@gmail.com', 9448348128, 2, 'bunder,statebank bus stand,manglore 01', 'karkera2984');

-- --------------------------------------------------------

--
-- Table structure for table `movement_register`
--

CREATE TABLE IF NOT EXISTS `movement_register` (
  `sr_no` int(11) NOT NULL AUTO_INCREMENT,
  `mr_name` text NOT NULL,
  `mr_image` varchar(65) NOT NULL,
  `mr_qual` varchar(20) NOT NULL,
  `mr_desig` varchar(20) NOT NULL,
  `mr_training` varchar(10) NOT NULL,
  `mr_pname` varchar(20) NOT NULL,
  `mr_paddress` varchar(100) NOT NULL,
  `mr_securitydocno` varchar(20) NOT NULL,
  `mr_oftelno` varchar(20) NOT NULL,
  `mr_pfno` varchar(20) NOT NULL,
  `mr_esino` int(11) NOT NULL,
  `mr_dob` date NOT NULL,
  `mr_doj` date NOT NULL,
  `mr_comeidno` varchar(20) NOT NULL,
  `mr_koelticketno` varchar(20) NOT NULL,
  `mr_pulseuidno` varchar(11) NOT NULL,
  `mr_tkissuedby` text NOT NULL,
  PRIMARY KEY (`sr_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `movement_register`
--

INSERT INTO `movement_register` (`sr_no`, `mr_name`, `mr_image`, `mr_qual`, `mr_desig`, `mr_training`, `mr_pname`, `mr_paddress`, `mr_securitydocno`, `mr_oftelno`, `mr_pfno`, `mr_esino`, `mr_dob`, `mr_doj`, `mr_comeidno`, `mr_koelticketno`, `mr_pulseuidno`, `mr_tkissuedby`) VALUES
(6, 'Ashwin', '14671-img-20180218-wa0098.jpg', 'Mcom', 'Servicer', 'None', 'Annappa', 'Kalasa', '9219873', '9012632137', '8928716', 728738721, '2018-03-14', '2018-03-20', 'hq2uye12', 'wefwe', 'A1002', 'NO'),
(7, 'Jeevan Poojary', '64972-65975-img_20170606_083130.jpg', 'MCA', 'Tecnician', '2 month', 'Umar Koya', 'Shivamogga', 'S21092', '9448348128', 'Jp0192', 117280, '1994-01-10', '2017-07-12', 'PPE2109', 'K10927', 'A1003', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(65) NOT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`news_id`, `email`) VALUES
(1, 'iamanismadi@gmail.com'),
(2, 'arshadarshu028@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_name` varchar(35) NOT NULL,
  `email_id` varchar(65) NOT NULL,
  `phone` bigint(15) NOT NULL,
  `address` varchar(100) NOT NULL,
  `qty` bigint(5) NOT NULL,
  `order_date` date NOT NULL,
  `user_email` varchar(65) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `product_id`, `user_name`, `email_id`, `phone`, `address`, `qty`, `order_date`, `user_email`) VALUES
(1, 27, 'ashraf', 'ashraf@gmail.com', 9988998899, 'Manglore', 1, '2018-04-04', 'a@gmail.com'),
(2, 41, 'Abhinandhan', 'abhi@gmail.com', 9448348128, 'manglore', 2, '2018-04-04', 'ashiq@gmail.com'),
(3, 34, 'Ashraf', 'Ash@gmail.com', 998898898, 'Manglore', 2, '2018-04-05', 'ashiq@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `org_id` int(11) NOT NULL AUTO_INCREMENT,
  `org_name` varchar(30) NOT NULL,
  `org_email` varchar(30) NOT NULL,
  `org_phone` bigint(20) NOT NULL,
  `org_address` varchar(100) NOT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`org_id`, `org_name`, `org_email`, `org_phone`, `org_address`) VALUES
(2, 'Prasad Power Engine pvt ltd', 'ppe@gmail.com', 8977626635, 'Yayyadi,Mnagluru\r\n575009'),
(3, 'prakash pvt lmt', 'p@gmail.com', 99882992, 'Kalasa'),
(4, 'karkera Org pvt lmt', 'karkera@gmail.com', 9448348128, 'bunder,statebank bus stand,manglore 01');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_title` varchar(30) NOT NULL,
  `product_image` varchar(65) NOT NULL,
  `product_type` text NOT NULL,
  `sub_type` varchar(65) NOT NULL,
  `product_description` varchar(5000) NOT NULL,
  `product_price` bigint(30) NOT NULL,
  `showroom_email` varchar(65) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title`, `product_image`, `product_type`, `sub_type`, `product_description`, `product_price`, `showroom_email`) VALUES
(34, 'Chota Chilli 2.1 kW - 4 kW', '18396-2.1-kw-4-kw.png', 'Petrol', 'Chota Chilli 2.1 Kw - 4 kw', 'Features\r\nRated/Max Power Output (VA) :2100/2400 VA\r\nInstallation & Commissioning :Labour Free on-site\r\nCompliance :Latest Government Emission Norms CPCB II\r\nStart Type :Easy Key Start\r\nService :Labour free onsite service under warranty period (420+ service touch points and 6000+ KOEL trained service professionals on field )', 84000, 'koelcare@gmail.com'),
(35, '3 kVA - 5 kVA', '31838-3-kva-5-kva.png', 'Diesel', '3KVA - 5 KVA', 'Rated/Max Power Output (VA) :	3000 VA\r\nInstallation & Commissioning :	Labour Free on-site\r\nCompliance :	Latest Government Emission Norms CPCB II\r\nStart Type :	Controller Based Easy Push Start\r\nService :	Labour free onsite service under warranty period (420+ service touch points and 6000+ KOEL trained service professionals on field)\r\n', 150000, 'koelcare@gmail.com'),
(36, '5 kVA - 12.5 kVA Slim Power', '29242-5-kva-12.5-kva-slim.png', 'Diesel', '5KVA - 12.5 KVA Slim Power', 'Compact Product:	Unmatched space efficiency\r\nLow OPEX:	Best in class fuel efficiency with O2E technology\r\nRobust and Reliable Product:	Lowest maintenance and trouble free product with Kirloskar Air Cooled Engines\r\nCompliance:	Latest Government Emission Norms CPCB II\r\nStart Type:	Controller Based Easy Push Start Type\r\nCustomer Service:	Quick and affordable door step service coverage (420+ service touch points and 6000+ KOEL trained service professionals on field)\r\nWarranty:	2 years warranty* & labour free onsite service under warranty period\r\n', 200000, 'koelcare@gmail.com'),
(37, '15 kVA - 20 kVA', '71936-15-kva-20-kva.png', 'Diesel', '15KVA - 20 KVA', 'Low OPEX:Best in class fuel efficiency with O2E technology\r\nRobust and Reliable Product:Lowest maintenance and trouble free product with Kirloskar Engines\r\nOption of Air Cooled and Water Cooled Engines\r\nCompliance:Latest Government Emission Norms CPCB II\r\nStart Type:Controller Based Easy Push Start Type\r\nCustomer Service:Quick and affordable door step service coverage (420+ service touch points and 6000+ KOEL trained service professionals on field)\r\nWarranty:2 years warranty & labour free onsite service under warranty period', 250000, 'koelcare@gmail.com'),
(38, '15 kVA - 30 kVA', '95396-15-kva-30-kva.png', 'Diesel', '15KVA - 30 KVA', 'Low OPEX:	Best in class fuel efficiency with O2E technology\r\nRobust and Reliable Product:	Lowest maintenance and trouble free product with Kirloskar Engines\r\nOption of Air Cooled and Water Cooled Engines	\r\nCompliance:	Latest Government Emission Norms CPCB II\r\nStart Type:	Controller Based Easy Push Start Type\r\nCustomer Service:	Quick and affordable door step service coverage (420+ service touch points and 6000+ KOEL trained service professionals on field)\r\nWarranty:	2 years warranty & labour free onsite service under warranty period\r\n', 300000, 'koelcare@gmail.com'),
(39, '40 kVA - 125 kVA', '62358-40-kva-125-kva.png', 'Diesel', '40KVA - 125 KVA', 'Low OPEX:	Best in class fuel efficiency with O2E technology\r\nRobust and Reliable Product:	Lowest maintenance and trouble free product with Kirloskar Engines\r\nOption of Air Cooled and Water Cooled Engines	\r\nCompliance:	Latest Government Emission Norms CPCB II\r\nStart Type:	Controller Based Easy Push Start Type\r\nCustomer Service:	Quick and affordable door step service coverage (420+ service touch points and 6000+ KOEL trained service professionals on field)\r\nWarranty:	2 years warranty & labour free onsite service under warranty period\r\n', 400000, 'koelcare@gmail.com'),
(40, '160 kVA - 250 kVA', '10057-160-kva-250-kva.png', 'Diesel', '160KVA - 250 KVA', 'Low OPEX:	Best in class fuel efficiency with O2E technology\r\nRobust and Reliable Product:	Lowest maintenance and trouble free product with Kirloskar Liquid Cooled Engines\r\nStart Type:	Controller Based Easy Push Start Type\r\nCustomer Service:	Quick and affordable door step service coverage (420+ service touch points and 6000+ KOEL trained service professionals on field)\r\nWarranty:	2 years warranty & labour free onsite service under warranty period\r\nKRM:	Critical genset parameters can be views on laptop or mobile\r\n', 500000, 'koelcare@gmail.com'),
(41, '320 kVA - 1010 kVA', '91048-320-kva-1010-kva.png', 'Diesel', '320KVA - 1010 KVA', 'Lowest OPEX:	Best in class fuel efficiency with O2E technology\r\nLowest Footprint:	Smallest footprint in this range while meeting the performance & CPCB II Norms\r\nModular Control System :	Easily adapted to any configuration, viz standalone, interlocked, synchronised or grid connected\r\nKOEL Green Alternator :	Built on globally renowned design. Patented winding design helps withstand operating harsh conditions.\r\nKRM :	Critical genset parameters can be viewed on laptop or mobile\r\nCustomer Service:	Quick and affordable door step service coverage (420+ service touch points and 6000+ KOEL trained service professionals on field)\r\nWarranty:	2 years warranty* & labour free onsite service under warranty period\r\n', 2000000, 'koelcare@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE IF NOT EXISTS `request` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `r_name` text NOT NULL,
  `r_phone` bigint(20) NOT NULL,
  `r_email` varchar(30) NOT NULL,
  `r_type` text NOT NULL,
  `r_kva` varchar(20) NOT NULL,
  `r_address` varchar(100) NOT NULL,
  `r_date` date NOT NULL,
  `user_email` varchar(30) NOT NULL,
  PRIMARY KEY (`r_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`r_id`, `r_name`, `r_phone`, `r_email`, `r_type`, `r_kva`, `r_address`, `r_date`, `user_email`) VALUES
(1, 'arshad', 98899889, 'ar@gmail.com', 'Desel', '5 kVa -12.5 slim kVa', 'Manglore', '2018-04-04', 'm@gmail.com'),
(2, 'arshad', 98899889, 'ar@gmail.com', 'Desel', '5 kVa -12.5 slim kVa', 'Manglore', '2018-04-04', 'm@gmail.com'),
(3, 'Akash', 9448348128, 'akash@gmail.com', 'Petrol', 'Chota Chilli 2kW -4 ', 'Manglore', '2018-04-04', 'ashiq@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `showroom`
--

CREATE TABLE IF NOT EXISTS `showroom` (
  `srm_id` int(11) NOT NULL AUTO_INCREMENT,
  `srm_name` varchar(20) NOT NULL,
  `srm_email` varchar(30) NOT NULL,
  `srm_phone` bigint(20) NOT NULL,
  `srm_address` varchar(100) NOT NULL,
  `srm_city` varchar(30) NOT NULL,
  `srm_district` varchar(30) NOT NULL,
  `srm_password` varchar(30) NOT NULL,
  PRIMARY KEY (`srm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `showroom`
--

INSERT INTO `showroom` (`srm_id`, `srm_name`, `srm_email`, `srm_phone`, `srm_address`, `srm_city`, `srm_district`, `srm_password`) VALUES
(1, 'KOEL CARE', 'koelcare@gmail.com', 9900990099, 'Mangalore', 'Mangalore', 'Dakshina Kannada', ' '),
(2, 'PPE TRADRERS', 'ppet@gmail.com', 9900889900, 'Manglore ', 'manglore', 'Karnataka', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `supervisor`
--

CREATE TABLE IF NOT EXISTS `supervisor` (
  `sup_id` int(11) NOT NULL AUTO_INCREMENT,
  `sup_name` text NOT NULL,
  `sup_phone` bigint(15) NOT NULL,
  `sup_dob` varchar(15) NOT NULL,
  `sup_email` varchar(50) NOT NULL,
  `sup_gender` varchar(8) NOT NULL,
  `sup_address` varchar(100) NOT NULL,
  `sup_quali` varchar(8) NOT NULL,
  `sup_exp` varchar(20) NOT NULL,
  `sup_password` varchar(30) NOT NULL,
  PRIMARY KEY (`sup_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `supervisor`
--

INSERT INTO `supervisor` (`sup_id`, `sup_name`, `sup_phone`, `sup_dob`, `sup_email`, `sup_gender`, `sup_address`, `sup_quali`, `sup_exp`, `sup_password`) VALUES
(4, 'Arshad', 9900990099, '2018-02-15', 'arshad@gmail.com', 'Male', '  Mangalore', 'MBA', '2 years', ' '),
(5, 'Abhinandhan', 9448348128, '2018-03-13', 'arshadarsu028@gmail.com', 'Male', '  Managluru', 'Mcom', 'none', ' '),
(7, 'Ashraf', 8951100737, '2018-03-15', 'mashraf@gmail.com', 'Male', '  Manglore', 'Bca', '2 years', ' ');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(10) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(30) NOT NULL,
  `u_phone` bigint(15) NOT NULL,
  `u_date` date NOT NULL,
  `u_gender` text NOT NULL,
  `u_address` varchar(100) NOT NULL,
  `u_email` varchar(20) NOT NULL,
  `u_password` varchar(30) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `u_name`, `u_phone`, `u_date`, `u_gender`, `u_address`, `u_email`, `u_password`) VALUES
(1, 'arshad', 9889988998, '2018-04-10', '', 'Kalaska', 'a@gmail.com', 'arshad'),
(2, 'Manamohan', 988998839, '2018-04-14', 'Male', 'Manglore', 'm@gmail.com', 'm12345'),
(3, 'Ashiq', 9448348128, '2018-04-09', 'Male', 'Manglore', 'ashiq@gmail.com', 'ashiq');
