<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head>
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
		

	
<!--content-->
<div class="container">
		<div class="account">
		<h1>ORDER PRODUCT</h1>
		<div class="account-pass">
		<div class="col-md-6 col-md-offset-3 account-top">
			<?php
				if(isset($_GET['order_success']))
				{
					echo '<div class="alert alert-info">
							<a href="#" class="close" data-dismiss="alert">&times</a>
							<p>We have recieved your order, we will notify about the product shortly...</p>
						</div>';
				}
			?>
			<form name="" method="post" action="order_val.php">
				<div> 	
					<span>Name</span>
					<input type="text" name="name"  placeholder="Name" required/>
					<input type="hidden" name="product_id" value="<?php echo $_GET['product_id']; ?>"  placeholder="Name" required/>
				</div>
				<div> 
					<span >Email</span>
					<input type="text" name="email" placeholder="Your Email" required>
				</div>	
				<div> 
					<span >Phone</span>
					<input type="text" name="phone"  placeholder="Phone" required/>
				</div>		
				<div> 
					<span >Address</span>
					<textarea name="address" rows="6" placeholder="Address" required></textarea>
				</div>	
				<div> 
					<span >Quantity</span>
					<input type="text" name="qty" placeholder="Number of Qty to be Purchased" required/>
				</div>	

				<input type="submit" class="btn-block btn" style="width:100%;" value="Proceed to Order" name="order_btn"> 					
			</form>
			<br/>
		</div>
		
	<div class="clearfix"> </div>
	</div>
	</div>

</div>

<?php include("inc/footer.php"); ?>
</body>
</html>
			