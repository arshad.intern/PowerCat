
<?php include("inc/session.php"); ?>

<!DOCTYPE html>
<html lang="en">

<head>

   <?php include("inc/head.php"); ?>

</head>

<body>
   <?php include("inc/topmenu.php"); ?>
    <!-- Navigation -->
      <div class="container-fluid main-container">
        <div class="col-md-2 sidebar">
            <?php include("inc/sidemenu.php"); ?>
        </div>
        <div class="col-md-10 content">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span> Dashboard - [ Logged in as <span style="color:yellow; font-weight:600;"><?php echo $email; ?> </span>]
                        <span class="pull-right">
                           <?php
                                if($user_role == 1)
                                {
                                    echo 'Admin';
                                }
                                else if($user_role ==2)
                                {
                                    echo 'Organization';
                                }
                                 else if($user_role ==3)
                                {
                                    echo 'Complaint Reciever';
                                }
                                else if($user_role ==4)
                                {
                                    echo 'Supervisor';
                                }
                                 else if($user_role ==5)
                                {
                                    echo 'ShowRoom';
                                }
                            ?>
                        </span>
                    </span>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered" id="example">
                    <thead>
                        <tr>
                            <th>Sno</th>
                            <th>Name</th>
							<th>Product Image</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Ganaretor Type</th>
                            <th>KVA Size</th>
                            <th>Action/th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php
                            include("../admin/connection.php");
                            $i = 1;
                            $query = mysqli_query($con, "SELECT * FROM `request`") or die(mysqli_error($con));
                            while($row = mysqli_fetch_array($query))
                            {
                              		echo '<tr>
                                        <td>'.$i++.'</td>
                                        <td>'.$row['r_name'].'</td>
                                        <td>'.$row['r_phone'].'</td>
                                        <td>'.$row['r_email'].'</td>
                                        <td>'.$row['r_type'].'</td>
                                        <td>'.$row['r_kva'].'</td>
                                        <td>
                                            <a href="products_val.php?product_id='.$row['req_id'].'&delete" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                        </td>
                                    </tr>';
                            }                   
                        ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
       <?php include("inc/footer.php"); ?>
    </div>
    <!- - jQuery Version 1.11.1 -->
    <?php include("inc/bottom.php"); ?>

</body>
</html>
