	<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head>
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
		<div class="contact">
			<div class="container">
				<h1 style="color:green;">Petrol Generator</h1>
				<div class="ul-side">
					<p>KOEL Chhota Chilli Portable Petrol Generators come from the house of Kirloskar, India’s #1 
					Generator brand. Specialized in manufacturing of generators for more than seven decades. This all
					new Petrol Generator is the latest addition to this widest portfolio. With KOEL Chota Chilli enjoy 
					the beneﬁts of mobility and portability. KOEL Chhota Chilli rolls on wheels and is equipped with a 
					full-width handle. Now you carry power with you for a beach party or a jungle camp. This feature is 
					of great help for small establishments and mobile workshops too!.
					KOEL Chhota Chilli Generators reach you through the widest network of authorized dealers and are 
					supported by over 5000 skilled service engineers spread across India. Quality of service and 
					response is centrally monitored by KOEL. Dial the toll-free number and our authorized representative
					will be promptly available at your door step!
					<h3 style="margin-top:20px;">Promise behind Product</h3>
					<ul class="ul-side">
						<li>Easy Key Start.</li>
						<li>Stable Output with Silent Design.</li>
						<li>Highest Rated Power Output.</li>
						<li>Easy Maneuverability.</li>
						<li>International Standard Output Plug.</li>
						<li>Recoil Start as Standard Option.</li>
						<li>Compliant to revised emission norms.</li>
					</ul>
					<p>Portable Petrol generator range that fulfills all your requirement. When it comes to Power, no one knows Generators better than us.</p>

					</p>
				</div>

				<div class="row">
					<div class="col-md-12">
						<img class="img-responsive center-block" style="width:50%;" src="images/2.1-kw-4-kw.png">
						<h2 class="text-center">2.1-kw-4-kw</h2>    
					</div>
				</div>
			</div>
		</div>
	
<!------ Include the above in your HEAD tag ---------->


	<?php include("inc/footer.php"); ?>
</body>
</html>