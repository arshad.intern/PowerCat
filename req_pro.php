<?php include("inc/session.php"); ?><?php
	if ($_SESSION['u_email'] == '')
	{
		header("location:login.php?logged_in");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head>
<body>
<!--header-->
	<?php include("inc/topmenu.php"); ?>
	
<!--content-->
<div class="container">

	<!-- //breadcrumbs --> 
	<!-- mobiles -->
	
	<div class="container">
		<div class="account">
		<h1>Request Your Products</h1>
		<div class="account-pass">
		<div class="col-md-6 col-md-offset-3 account-top">

						<?php
							if(isset($_GET['req_success']))
							{
								echo '<div class="alert alert-success p-3">
										<a href="#" class="close" data-dismiss="alert">&nbsp;</a>
										<p class="p-2">Successfully.. Requested the product..</p>
									</div>';
							}
						?>
			           <form name="" method="post" action="req_val.php">

	                            <label>Name:</label>
	                           		<input type="text" name="r_name" placeholder="Enter Name" required/>
	                            <label>Phone:</label>
                                    <input type="text" name="r_phone"  placeholder="Enter Phone" required/>
                                <label>Email:</label>
                                    <input type="email" name="r_email" placeholder="Enter Email" required/>
                               
                                <label> Ganaretor Type:</label>
                                    <select  name="r_type" required>
                                        <option>select</option>
                                        <option>Desel</option>
                                        <option>Petrol</option>
                                    </select>
                                   <label> KVA Size:</label>
                                    <select name="r_kva" required>
                                        <option>select</option>
                                        <option>Chota Chilli 2kW -4 kW</option>
                                        <option>3 kVa -5 kVa</option>
                                        <option>5 kVa -12.5 slim kVa </option>
                                        <option>15 kVa -20 kVa</option>
                                        <option>15 kVa -30 kVa </option>
                                        <option>40 kVa -125 kVa</option>
                                        <option>160 kVa - 250</option>
                                        <option>320 kVa - 1010</option>

                                    </select>
                                
                                <label>Address:</label>
                                    	<textarea name="r_address" rows="5"  placeholder="Enter  Address" required></textarea>
                                <div>
                                    <input type="submit" name="req_prod" class="btn-block btn" style="width:100%;"  value="Request Your Products"/>
                                </div>
                            </form>
                        </div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>  

	<?php include("inc/footer.php"); ?> 
</body>
</html>