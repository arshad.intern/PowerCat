	<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head>
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
		<div class="contact">
			<div class="container">
				<h1 style="color:blue;">Diesel Generator</h1>
				<div class="ul-side">
					<p>
						KOEL Chhota Chilli and KOEL Green are the Diesel Genset brands of Kirloskar Oil Engines Ltd (KOEL),
						the flagship company of the century-old Kirloskar Group. KOEL is India’s largest selling and most trusted DG Set Manufacturer in 
						India for over a decade. Providing back-up power solutions from 2.1 to 5200 kVA for diverse market sectors, KOEL Green and KOEL 
						Chhota Chilli have over 1 million Gensets in service across the globe.<br><br><br>
						KOEL Silent Diesel Generator are designed and developed indigenously, using modern design &amp; simulation technologies. 
						KOEL Green is one of the most competent Diesel Generator Set (Genset) manufacturers in India, which is designed to provide 
						optimum power backup solutions in various business and commercial applications. KOEL's R&amp;D team combines decades of application 
						knowledge, global technology trends and emerging user expectations to develop best-in-class products for the target markets.
						The products are launched after extensive validation in world-class facilities.
					</p>
				</div>

				<div class="row">
					<div class="col-md-3">
						<img class="img-responsive" src="images/3-kva-5-kva.png">
						<h2>3 kVa - 5 kVa</h2>
					</div>
					<div class="col-md-3">
						<img class="img-responsive" src="images/5-kva-125-kva-slim.png">
						<h2>5 kVa - 12.5 slim kVa</h2>
					</div>
					<div class="col-md-3">
						<img class="img-responsive" src="images/15-kva-20-kva.png">
						<h2>15 kVa - 20 kVa</h2>
					</div>
					<div class="col-md-3">
						<img class="img-responsive" src="images/15-kva-30-kva.png">
						<h2>15 kVa - 30 kVa</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<img class="img-responsive" src="images/40-kva-125-kva.png">
						<h2>40 kVa - 125 kVa</h2>
					</div>
					<div class="col-md-3">
						<img class="img-responsive" src="images/160-kVa-250-kva.png">
						<h2>160 kVa - 250</h2>
					</div>
					<div class="col-md-3">
						<img class="img-responsive" src="images/320-kva-1010-kva.png">
						<h2>320 kVa - 1010</h2>
					</div>
				</div>
			</div>
		</div>
	
	<?php include("inc/footer.php"); ?>
</body>