<?php include('inc/session.php')?>
<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head>
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
		<div class="contact">
			<div class="container">
				<h1>About Us</h1>
						<div class="ul-side">
							<p>Prasad Power Engineers (PPE) is an ISO 9001-2008 certified organization, delivering excellence in the service industry and providing multifarious engineering solutions to itsEsteemed Customers since 2002. To reiterate PPE is the sole Authorized KOEL Care Centre for Kirloskar Oil Engines Ltd. (KOEL) in the entire Dakshina Kannada, Udupi, Shivamogga andChikmagalurDistricts.</p>
						</div>
							<!-- <div class="col-xs-2 w3ls_about_grid_left1">
								<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
							</div> -->
							<div class="ul-side">
								<p>We are specialized in the field of Commissioning, Preventive Maintenance, Servicing and Overhauling of all Kirloskar Make Diesel&amp; Petrol Engines. Our business objective is to gain utmost customer satisfaction as we trust that service satisfaction is the strongest basis for business excellence. We always strive to draw the attention of customers with our reliable services that are offered in consideration to the client requirements. Towards this, we have been positioning adequate KOEL trained man power with sufficient stocks of Genuine Spare Parts, consumables, special tools, etc. for ensuring efficient and round the clock service support to over 9000+ Kirloskar end users with a service engineer’s ratio of 100:1. We pursue timely delivery policy that makes sure in-time delivery of the quality service to our customers. We are acknowledged as the best dealers by our clients due to our flexible, transparent, innovative and personal approach. Our company is the one stop destination for our customers to avail the services with the best quality at affordable prices.</p>
							</div>
						</div>
					</div>
					<div class="mail">
						<div class="container">
							<div class="ul-side">
								<h3>Our Promise</h3>
									<div class="ul-side" style="color:#999; line-height:1.8em; margin-bottom:2em;">
										<ul>
												<li>Proactiveness : Action Beforehand.</li>
												<li>Speed : Prompt Response and Restoration.</li>
												<li>First Time Right : Do it Right Every Single Time.</li>
												<li>One Stop Solution : Everything under One Roof.</li>
												<li>Service Ownership : Complete Customer Ownership.</li>
											</ul>
									</div>
							</div>
							<div class="content-bottom">
		<h1 class="text-center" style="margin-bottom: 60px;">OUR ACHIVEMENTS</h1>
		<ul>
			<li><a href="#"><img class="img-responsive" src="images/achivements/Award-3.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/achivements/Award-4.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/achivements/Award-6.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" style="width: 200px;height: 170px;" src="images/achivements/award-20.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" style="width: 240px;height: 170px;"src="images/achivements/P2.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/achivements/P5.jpg" alt=""></a></li>
		<div class="clearfix"> </div>
		</ul>
		<h1 class="text-center" style="margin-bottom: 60px;margin-top: 60px;">OUR CLIENTS</h1>
		<ul>
			<li><a href="#"><img class="img-responsive" src="images/clients/bharath-bank.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/Canara-Bank.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/SDM.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/Infosys.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/ICICI_Bank.jpg" alt=""></a></li>
			<li><a href="#"><img class="img-responsive" src="images/clients/syndicate-bank.jpg" alt=""></a></li>
		<div class="clearfix"> </div>
		</ul>
	</div>
						</div>
						
						</div>
						<!-- //about --> 
						
						</div>
						</div>
					<?php include("inc/footer.php"); ?>
</body>