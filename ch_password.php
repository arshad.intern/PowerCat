<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("inc/head.php"); ?>
    <script>
    function validatePassword() {
    var currentPassword,newPassword,confirmPassword,output = true;

    currentPassword = document.frmChange.currentPassword;
    newPassword = document.frmChange.newPassword;
    confirmPassword = document.frmChange.confirmPassword;

    if(!currentPassword.value) {
        currentPassword.focus();
        document.getElementById("currentPassword").innerHTML = "required";
        output = false;
    }
    else if(!newPassword.value) {
        newPassword.focus();
        document.getElementById("newPassword").innerHTML = "required";
        output = false;
    }
    else if(!confirmPassword.value) {
        confirmPassword.focus();
        document.getElementById("confirmPassword").innerHTML = "required";
        output = false;
    }
    if(newPassword.value != confirmPassword.value) {
        newPassword.value="";
        confirmPassword.value="";
        newPassword.focus();
        document.getElementById("confirmPassword").innerHTML = "not same";
        output = false;
    }   
    return output;
}
</script>
</head> 
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
         <?php
        include("connection.php");
        if(count($_POST)>0) {
            $result = "SELECT * from `user` WHERE u_email='$user_email'";
            $m=mysqli_query($con, $result);
            $row=mysqli_fetch_array($m);
            if($_POST["currentPassword"] == $row["u_password"]) {
            $e="UPDATE user set u_password='" . $_POST["newPassword"] . "' WHERE u_email='$user_email'";
            $res=mysqli_query($con,$e);
            $message = "Password Changed";
            } else $message = "Current Password is not correct";
        }
    ?>   
    <?php
        /*if(!($_SESSION['u_email']))
        {
            echo '<script>
                        alert("Login First to continue order...");
                        window.location.href="index.php";
                    </script>';
        }*/
    ?>
		<div class="contact">
			<div class="container">
				<h2 style="text-align:center;margin-bottom:10px;">Change Password</h2>
           
                <div class="w3ls_about_grids"></div>
                    <div class="mobiles">
                        <div class="container">
                            <div class="w3ls_mobiles_grids">
                                <div class="col-md-12 w3ls_mobiles_grid_right">
                                    <div class="clearfix"> </div>
                                        <div class="col-md-12">
                                          <form name="frmChange" method="post" action="" onSubmit="return validatePassword()">

                                                <?php if(isset($message)) {
                                                    echo '<div class="alert alert-info">'.$message.'</div>'; 
                                                    }
                                                ?>
                                                <table class="table table-bordered">
                                                    <tr class="tableheader">
                                                        <td colspan="2"><h5> Change Password </h5></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%"><label> Current Password </label></td>
                                                        <td width="60%"><input type="password" name="currentPassword" class="form-control"/><span id="currentPassword"  class="required"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td><label> New Password </label></td>
                                                        <td><input type="password" name="newPassword" class="form-control"/><span id="newPassword" class="required"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td><label> Confirm Password </label></td>
                                                        <td><input type="password" name="confirmPassword" class="form-control"/><span id="confirmPassword" class="required"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="text-align: center;"><input type="submit" name="submit" value="Submit" class="btn btn-primary"></td>
                                                    </tr>
                                                </table>
                                            </form>
                                         </div>
                                    <div class="w3ls_mobiles_grid_right_grid3">
									<div class="clearfix"> </div>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
    <?php include("inc/footer.php"); ?> 
</body>
</html>