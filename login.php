<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
</head>
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
		

	
<!--content-->
<div class="container">
		<div class="account">
		<h1>Login</h1>
		<div class="account-pass">
		<div class="col-md-6 col-md-offset-3 account-top">
			
			<?php
				if(isset($_GET['login_first']))
				{
					echo '<div class="alert alert-danger">
							<a href="#" class="close" data-dismiss="alert">&times</a>
							<p>You Must Login First to Continue...</p>
						</div>';
				}
				else if(isset($_GET['logged_out']))
				{
					echo '<div class="alert alert-info">
							<a href="#" class="close" data-dismiss="alert">&times</a>
							<p>You have been logged out...</p>
						</div>';
				}		
			?>
			<form name="" method="post" action="">
				
			<div> 	
				<span>Email</span>
				<input type="text" name="u_email"  placeholder="Your Email" required> 
			</div>
			<div> 
				<span >Password</span>
				<input type="password" name="u_password" placeholder="Your Password" required>
			</div>				
				<input type="submit" class="btn-block btn" style="width:100%;" value="Login" name="login_btn"> 
			</form>
			<br/>

			<p>Didn't Register Yet? <a href="register.php">Register</a><span class="pull-right"><a href="forgot.php">ForgotPassword?</a></span></p>
			<?php
			include("connection.php");
			if(isset($_POST['login_btn']))
			{
				$u_email = mysqli_real_escape_string($con, $_POST['u_email']);
				$u_password = mysqli_real_escape_string($con, $_POST['u_password']);
				$sql = mysqli_query($con, "SELECT * FROM `user` WHERE u_email = '$u_email' AND u_password = '$u_password'") or die(mysqli_error($con));
				$row = mysqli_fetch_array($sql);
				if($row)
				{
					$_SESSION['u_email'] = $row['u_email'];
					header("location:index.php?logged_in");
				}
				else
				{
					echo '<h4 class="text-center" style="color:red; text-align:center;">Invalid Email Address and Password...</h4>';
				}
			}
		
		
		?>
		</div>
		
	<div class="clearfix"> </div>
	</div>
	</div>

</div>

<?php include("inc/footer.php"); ?>
</body>
</html>
			