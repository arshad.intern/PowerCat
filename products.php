<?php include("inc/session.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<?php include("inc/head.php"); ?>
	<style type="text/css">
		.img { 
			position: relative; 
			width: 100%; 
			height: 350px; 
			background-position: 50% 50%; 
			background-repeat: no-repeat; 
			background-size: cover; 
		} 
	</style>
</head>
<body>
<!--header-->

		<?php include("inc/topmenu.php"); ?>
		

	

	
<!--content-->
<!---->
		<div class="product">
			<div class="container">
				<div class="col-md-3 product-price">
					  
				<div class=" rsidebar span_1_of_left">
					<div class="of-left">
						<h3 class="cate">Categories</h3>
					</div>
		 <ul class="menu">
		<li class="item1"><a href="#">Petrol </a>
			<ul class="cute">
				<li class="subitem1"><a href="products.php?sub_cat_id=1">Chota Chilli 2.1 Kw - 4 kw</a></li>
			</ul>
		</li>
		<li class="item2"><a href="#">Diesel </a>
			<ul class="cute">
				<li class="subitem1"><a href="products.php?sub_cat_id=2">3KVA - 5 KVA </a></li>
				<li class="subitem2"><a href="products.php?sub_cat_id=3">5KVA - 12.5 KVA Slim Power </a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=4">15KVA - 20 KVA </a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=5">15KVA - 30 KVA</a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=6">40KVA - 125 KVA </a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=7">160KVA - 250 KVA</a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=8">320KVA - 1010 KVA</a></li>
			</ul>
		</li>
	
	</ul>
					</div>
				<!--initiate accordion-->
		<script type="text/javascript">
			$(function() {
			    var menu_ul = $('.menu > li > ul'),
			           menu_a  = $('.menu > li > a');
			    menu_ul.hide();
			    menu_a.click(function(e) {
			        e.preventDefault();
			        if(!$(this).hasClass('active')) {
			            menu_a.removeClass('active');
			            menu_ul.filter(':visible').slideUp('normal');
			            $(this).addClass('active').next().stop(true,true).slideDown('normal');
			        } else {
			            $(this).removeClass('active');
			            $(this).next().stop(true,true).slideUp('normal');
			        }
			    });
			
			});
		</script>

						<div class="sellers">
							<div class="of-left-in">
								<h3 class="tag">Tags</h3>
							</div>
								<div class="tags">
									<ul>
										<li><a href="products.php?sub_cat_id=1">2.1 Kw - 4 kw</a></li>
										<li><a href="products.php?sub_cat_id=2">3KVA - 5 KVA</a></li>
										<li><a href="products.php?sub_cat_id=3">5KVA - 12.5 KVA</a></li>
										<li><a href="products.php?sub_cat_id=4">15KVA - 20 KVA</a></li>
										<li><a href="products.php?sub_cat_id=5">15KVA - 30 KVA</a></li>
										<li><a href="products.php?sub_cat_id=6">40KVA - 125 KVA</a></li>
										<li><a href="products.php?sub_cat_id=7">160KVA - 250 KVA</a></li>
										<li><a href="products.php?sub_cat_id=8">320KVA - 1010 KVA</a></li>
										<div class="clearfix"> </div>
									</ul>
								
								</div>
								
		</div>

				</div>
				<div class="col-md-9 product1">
				<div class=" bottom-product">
				<?php
					include("admin/connection.php"); 
					$condition = "WHERE 1";
					if(isset($_GET['sub_cat_id']))
					{
						if($_GET['sub_cat_id'] == 1)
						{
							$condition = "WHERE sub_type = 'Chota Chilli 2.1 Kw - 4 kw'";
						}
						else if($_GET['sub_cat_id'] == 2)
						{
							$condition = "WHERE sub_type = '3KVA - 5 KVA'";
						}
						else if($_GET['sub_cat_id'] == 3)
						{
							$condition = "WHERE sub_type = '5KVA - 12.5 KVA Slim Power'";
						}
						else if($_GET['sub_cat_id'] == 4)
						{
							$condition = "WHERE sub_type = '15KVA - 20 KVA'";
						}
						else if($_GET['sub_cat_id'] == 5)
						{
							$condition = "WHERE sub_type = '15KVA - 30 KVA'";
						}
						else if($_GET['sub_cat_id'] == 6)
						{
							$condition = "WHERE sub_type = '40KVA - 125 KVA'";
						}
						else if($_GET['sub_cat_id'] == 7)
						{
							$condition = "WHERE sub_type = '160KVA - 250 KVA'";
						}
						else if($_GET['sub_cat_id'] == 8)
						{
							$condition = "WHERE sub_type = '320KVA - 1010 KVA'";
						}

						
					}


					if(isset($_POST['search_product']))
					{
						$product_title = $_POST['product_title'];
						$condition = "WHERE product_title LIKE '%$product_title%'";
					}

					$limit = 9;  
					if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
					$start_from = ($page-1) * $limit;  
					$sql = mysqli_query($con, "SELECT * FROM `products` $condition LIMIT $start_from, $limit") or die(mysqli_error($con));

					$count = mysqli_num_rows($sql);

					if($count > 0)
					{
						while($row = mysqli_fetch_array($sql))
						{

							$product_image = $row['product_image'];
							if($product_image == "")
							{
								$product_image = "";
							}
							else
							{
								$product_image = "admin/products/".$product_image;
							}
							echo '<div class="col-md-4 bottom-cd simpleCart_shelfItem" style="margin-bottom:12px;">
							<div class="product-at ">
								<a href="product_details.php?product_id='.$row[0].'"><img class="img-responsive center-block img" src="'.$product_image.'"  alt="">
								<div class="pro-grid">
											<span class="buy-in">Order Now</span>
								</div>
							</a>	
							</div>
							<p class="tun">'.$row['product_title'].'</p>
							<a href="#" class="item_add"><p class="number item_price"><i> </i>Rs. '.$row['product_price'].'.00</p></a>						
						</div>


						';
						}	
					}
					else
					{
						echo '
							<center><img class="img-responsive" width="300" height="300" src="./images/empty.svg" alt=""></center>
							<h2 style="color:red; text-align:center;margin-top:15px;">No products Found...</h2>
						';
					}
					
				?>

					
					
					
					<div class="clearfix"> </div>
				</div>
				
				</div>
		<div class="clearfix"> </div>
		<nav class="in">
				 
				  	<?php  
						$sql = "SELECT COUNT(product_id) FROM products $condition";  
						$rs_result = mysqli_query($con, $sql);  
						$row = mysqli_fetch_row($rs_result);  
						$total_records = $row[0];  
						$total_pages = ceil($total_records / $limit);  
						$pagLink = "<ul class='pagination'>";  
							for ($i=1; $i<=$total_pages; $i++) {  
             					$pagLink .= "<li><a href='products.php?page=".$i."'>".$i."</a></li>";  
							};  
							echo $pagLink . "</div>";  
						?>
				</nav>
		</div>
		
		</div>
			
				<!---->

<!--//content-->
<?php include("inc/footer.php"); ?>
</body>
</html>