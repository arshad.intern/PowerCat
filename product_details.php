<?php include("inc/session.php"); ?>

<!DOCTYPE html>
<html lang="en">

<head>

   <?php include("inc/head.php"); ?>

</head>

<body>
   <?php include("inc/topmenu.php"); ?>
	
<!--content-->
<!---->
		<div class="product">
			<div class="container">
				<div class="col-md-3 product-price">
						<div class=" rsidebar span_1_of_left">
					<div class="of-left">
						<h3 class="cate">Categories</h3>
					</div>
		 <ul class="menu">
		<li class="item1"><a href="#">Petrol </a>
			<ul class="cute">
				<li class="subitem1"><a href="products.php?sub_cat_id=1">Chota Chilli 2.1 Kw - 4 kw</a></li>
			</ul>
		</li>
		<li class="item2"><a href="#">Diesel </a>
			<ul class="cute">
				<li class="subitem1"><a href="products.php?sub_cat_id=2">3KVA - 5 KVA </a></li>
				<li class="subitem2"><a href="products.php?sub_cat_id=3">5KVA - 12.5 KVA Slim</a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=4">15KVA - 20 KVA </a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=5">15KVA - 30 KVA</a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=6">40KVA - 125 KVA </a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=7">160KVA - 250 KVA</a></li>
				<li class="subitem3"><a href="products.php?sub_cat_id=8">320KVA - 1010 KVA</a></li>
			</ul>
		</li>
	
	</ul>
					</div>
				<!--initiate accordion-->
		<script type="text/javascript">
			$(function() {
			    var menu_ul = $('.menu > li > ul'),
			           menu_a  = $('.menu > li > a');
			    menu_ul.hide();
			    menu_a.click(function(e) {
			        e.preventDefault();
			        if(!$(this).hasClass('active')) {
			            menu_a.removeClass('active');
			            menu_ul.filter(':visible').slideUp('normal');
			            $(this).addClass('active').next().stop(true,true).slideDown('normal');
			        } else {
			            $(this).removeClass('active');
			            $(this).next().stop(true,true).slideUp('normal');
			        }
			    });
			
			});
		</script>

						<div class="sellers">
							<div class="of-left-in">
								<h3 class="tag">Tags</h3>
							</div>
								<div class="tags">
									<ul>
										<li><a href="products.php?sub_cat_id=1">2.1 Kw - 4 kw</a></li>
										<li><a href="products.php?sub_cat_id=2">3KVA - 5 KVA</a></li>
										<li><a href="products.php?sub_cat_id=3">5KVA - 12.5 KVA</a></li>
										<li><a href="products.php?sub_cat_id=4">15KVA - 20 KVA</a></li>
										<li><a href="products.php?sub_cat_id=5">15KVA - 30 KVA</a></li>
										<li><a href="products.php?sub_cat_id=6">40KVA - 125 KVA</a></li>
										<li><a href="products.php?sub_cat_id=7">160KVA - 250 KVA</a></li>
										<li><a href="products.php?sub_cat_id=8">320KVA - 1010 KVA</a></li>
										<div class="clearfix"> </div>
									</ul>
								
								</div>
								
						</div>

				</div>


				<div class="col-md-9 product-price1">
					<?php
						include("admin/connection.php"); 
						$product_id = $_GET['product_id'];
						$sql = mysqli_query($con, "SELECT * FROM `products` WHERE product_id = '$product_id'") or die(mysqli_error($con));
						$row = mysqli_fetch_array($sql);
						$product_image = $row['product_image'];
						if($product_image == "")
						{
							$product_image = "";
						}
						else
						{
							$product_image = "admin/products/".$product_image;
						}
					?>
				<div class="col-md-5 single-top">	
			<div class="flexslider">
			  <ul class="slides">
			    <li data-thumb="<?php echo $product_image; ?>">
			      <img src="<?php echo $product_image; ?>" />
			    </li>
			    <li data-thumb="<?php echo $product_image; ?>">
			      <img src="<?php echo $product_image; ?>" />
			    </li>
			    <li data-thumb="<?php echo $product_image; ?>">
			      <img src="<?php echo $product_image; ?>" />
			    </li>
			    <li data-thumb="<?php echo $product_image; ?>">
			      <img src="<?php echo $product_image; ?>" />
			    </li>
			  </ul>
			</div>
<!-- FlexSlider -->
  <script defer src="js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

<script>
// Can also be used with $(document).ready()
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});
</script>
					</div>	
					<div class="col-md-7 single-top-in simpleCart_shelfItem">
						<div class="single-para ">
						<h4><?php echo $row['product_title']; ?></h4>
							
							
							<h5 class="item_price"><?php echo 'Rs. '.$row['product_price'].'.00'; ?></h5>
							<p><?php echo nl2br($row['product_description']); ?></p>
							<div class="available">
								<ul>
									<li>Product Type:
										<select disabled>
										<option><?php echo $row['product_type']; ?></option>
									</select></li>
								<li class="size-in">Sub Type:<select disabled>
									<option><?php echo $row['sub_type']; ?></option>
								</select></li>
								<div class="clearfix"> </div>
							</ul>
						</div>
							
								<a href="order.php?product_id=<?php echo $row['product_id']; ?>" class="add-cart item_add">ORDER NOW</a>
							
						</div>
					</div>
				<div class="clearfix"> </div>
			<!---->			
</div>

		<div class="clearfix"> </div>
		</div>
		</div>
<!--//content-->
<?php include("inc/footer.php"); ?>
</body>
</html>